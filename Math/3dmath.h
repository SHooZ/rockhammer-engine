#ifndef _3DMATH_H
#define _3DMATH_H

#include "../Render/CVector.h"
#include <float.h>
#include <math.h>
#define PI 3.1415926535897932		// ��� ���������� �� %)

// ������� ��� ���������. ��� ����� ������������� ��� ������������ �������� ClassifySphere().
#define BEHIND		0	// ���� ����� ������ ���������
#define INTERSECTS	1	// ���� ����� ���������� ���������
#define FRONT		2	// ���� ����� ������� ���������


// ��� ��������� ������������ ��� �������� 3� ����� � ��������. ��� ��������������
// � ����� ������ �� ������, ��������� �����, ����� ������ �����������.


// ���������� ������, ���������������� ���� ���������� �������� (���������)
CVector3 Cross(CVector3 vVector1, CVector3 vVector2);

// ���������� ������ ����� 2�� �������.
CVector3 Vector(CVector3 vPoint1, CVector3 vPoint2);

// ���������� �������� ������� ��� ������ ������� �������
float Magnitude(CVector3 vNormal);

// ���������� ��������������� ������ (��� ������ ���������� ������ 1)
CVector3 Normalize(CVector3 vNormal);

// ���������� ������� �������� (�����������, ���� �������� �������)
CVector3 Normal(CVector3 vTriangle[]);

// ���������� ���������� ��������� �� ������ ��������� (0,0,0).
// ��������� ������� � ��������� � ����� �����, ������� �� ���� ���������:
float PlaneDistance(CVector3 Normal, CVector3 Point);

// ��������� ����������� (���������) � �����, � ���������� true ��� �� �����������.
bool IntersectedPlane(CVector3 vPoly[], CVector3 vLine[], CVector3 &vNormal, float &originDistance);



// ���������� dot product ����� ����� ���������
float Dot(CVector3 vVector1, CVector3 vVector2);

// ���������� ���� ����� ����� ���������
double AngleBetweenVectors(CVector3 Vector1, CVector3 Vector2);

// ���������� ����� ����������� �������� � ����� (��������������� ����������� ���������)
CVector3 IntersectionPoint(CVector3 vNormal, CVector3 vLine[], double distance);

// ���������� true ���� ����� ����������� ��������� ������ ��������
bool InsidePolygon(CVector3 vIntersection, CVector3 Poly[], long verticeCount);

// ����������� ��� ������� ��� ����� ����������� ����� � ��������
bool IntersectedPolygon(CVector3 vPoly[], CVector3 vLine[], int verticeCount);

// ���������� ���������� �������� ����������� �����
float Absolute(float num);

//	��������� ���������� ������������ ���� ��������
float Dot(CVector3 vVector1, CVector3 vVector2);

// ���������� ��������� ����� ����� 3D �������
float Distance(CVector3 vPoint1, CVector3 vPoint2);

// ���������� ����� �� ������� ����� vA___vB, ������� ����� ����� � vPoint
CVector3 ClosestPointOnLine(CVector3 vA, CVector3 vB, CVector3 vPoint);

// ��� �-� �������� ���, ��������� �� ����� �������, �����, ��� ���������� ���������. �����������
// ��������  - ����� �����, ������� ���������, ����� ���������, ������ ����� � ���������� ���
// �������� ���������.
int ClassifySphere(CVector3 &vCenter,CVector3 &vNormal, CVector3 &vPoint, float radius, float &distance);

// ���������� true, ���� ����� ���������� ����� ����������� ������������. ��������� ����� �����,
// ������, ������� �������� � �� �����. ������� ���������� ������ ���� �� ������ ��������� �������:
// ����� ������������ �� ������������, �� ����� ��� ����� ���� ������ ����.
bool EdgeSphereCollision(CVector3 &vPosition,CVector3 vPolygon[], int vertexCount, float radius);

// ���������� true, ���� ����� ���������� ���������� �������. ��������� - ������� ��������,
// �� �����, ����� � ������ �����.
bool SpherePolygonCollision(CVector3 vPolygon[], CVector3 &vCenter,int vertexCount, float radius);

CVector3 AddVector(CVector3 vVector1, CVector3 vVector2);

CVector3 DivideVectorByScaler(CVector3 vVector1, float Scaler);

#endif
