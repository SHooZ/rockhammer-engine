#pragma once

#include <SDL/SDL.h>

#include <iostream>
class CRender
{
public:
	CRender();
	SDL_Renderer* createRenderer(SDL_Window*);
	//SDL_Viewport* createViewport();
	//SDL_Texture* LoadImage(std::string, SDL_Renderer*);
	bool enableVsync(bool);
	void render();
	~CRender();
};
