#include "CRender.h"
#include "CCamera.h"
#include <SDL/SDL_opengl.h>
#pragma comment(lib,"opengl32.lib")
#pragma comment (lib, "SDL2.lib")

CRender::CRender()
{
	GLenum error = GL_NO_ERROR;
	//Initialize ProjectionMatrix
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	//Check for error
	error = glGetError();
	if( error != GL_NO_ERROR )
	{
		std::cout<<"CRender::CRender - OpenGL init Error!"<<std::endl;
	}
	//Initialize Modelview Matrix
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	//Check for error
	error = glGetError();
	if( error != GL_NO_ERROR )
	{
		std::cout<<"CRender::CRender - OpenGL init Error!"<<std::endl;
	}
	glClearColor( 0.f, 0.f, 0.f, 1.f );
	//Check for error
	error = glGetError();
	if( error != GL_NO_ERROR )
	{
		std::cout<<"CRender::CRender - OpenGL init Error!"<<std::endl;
	}
	std::cout<<"CRender::CRender - OpenGL init success!"<<std::endl;
}

void CRender::render()
{
	//Clear color buffer
	glClear( GL_COLOR_BUFFER_BIT );

	//Render quad
	gluLookAt(CCamera.m_vPosition.x, g_Camera.m_vPosition.y, g_Camera.m_vPosition.z,
          g_Camera.m_vView.x,     g_Camera.m_vView.y,     g_Camera.m_vView.z,
          g_Camera.m_vUpVector.x, g_Camera.m_vUpVector.y,g_Camera.m_vUpVector.z);

		 glBegin (GL_TRIANGLES);
        glColor3ub(255, 0, 0);
        glVertex3f(0, 1, 0);

        glColor3ub(0, 255, 0);
        glVertex3f(-1, 0, 0);

        glColor3ub(0, 0, 255);
        glVertex3f(1, 0, 0);
    glEnd();
	
}


bool CRender::enableVsync(bool temp)
{
	if( SDL_GL_SetSwapInterval( temp ) < 0 )
			 {
				 std::cout<<"Warning: Unable to set VSync! SDL Error: %s\n"<< SDL_GetError()<<std::endl;
				 return false;
			 }
	return true;
}


SDL_Renderer* CRender::createRenderer(SDL_Window* win)
{
	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == NULL)
	{
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
	}
	std::cout<<"CRender::createRenderer - Renderer creating...\tOK"<<std::endl;
	return ren;
}


CRender::~CRender()
{

}
