#ifndef _CCAMERA_H
#define _CCAMERA_H

#include "../Window/CWindow.h"
#include "CVector.h"
#include <SDL\SDL_opengl.h>
#include <glut.h>
class CCamera 
{
private:
	CVector3 m_vPosition;
	CVector3 m_vView;
	CVector3 m_vUpVector;
	CVector3 m_vStrafe;
	unsigned int HEIGHT;
	unsigned int WIDTH;
public:
	CCamera(unsigned int, unsigned int);
	CVector3 Position() {	return m_vPosition;	}
	CVector3 View()		{	return m_vView;	}
	CVector3 UpVector() {	return m_vUpVector;	}
	CVector3 Strafe()	{	return m_vStrafe;}

	void PositionCamera(float positionX, float positionY, float positionZ,
				float viewX,     float viewY,     float viewZ,
				float upVectorX, float upVectorY, float upVectorZ);

	void RotateView(float angle, float X, float Y, float Z);
	void SetViewByMouse();

	void StrafeCamera(float speed);
	void MoveCamera(float speed);
	void CheckForMovement();
 
	void Update();
	void Look();
 
};

#endif
