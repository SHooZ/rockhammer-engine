#ifndef _CVECTOR_H
#define _CVECTOR_H

class CVector3 {    
public:
	CVector3() {}

	CVector3(float X, float Y, float Z)
	{
		x = X; y = Y; z = Z;
	}
	CVector3 operator+(CVector3 vVector)
	{
		return CVector3(vVector.x + x, vVector.y + y, vVector.z + z);
	}
	CVector3 operator-(CVector3 vVector)
	{
		return CVector3(x-vVector.x, y-vVector.y, z-vVector.z);
	}
	CVector3 operator*(float num)
	{
		return CVector3(x*num, y*num, z*num);
	}
	CVector3 operator/(float num)
	{
		return CVector3(x/num,y/num,z/num);
	}
 
	float x, y, z;				// ������ float ��� X,Y,Z
};

class CVector2{
    public:
	CVector2() {}

	CVector2(float X, float Y)
	{
		x = X; y = Y;
	}
	CVector2 operator+(CVector2 vVector)
	{
		return CVector2(vVector.x + x, vVector.y + y);
	}
	CVector2 operator-(CVector3 vVector)
	{
		return CVector2(x-vVector.x, y-vVector.y);
	}
	CVector2 operator*(float num)
	{
		return CVector2(x*num, y*num);
	}
	CVector2 operator/(float num)
	{
		return CVector2(x/num,y/num);
	}
 
	float x, y;				
};

#endif