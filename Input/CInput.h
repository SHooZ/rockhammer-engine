#pragma once

#include <SDL/SDL.h>

class CInput
{
private:
	SDL_Event e;
public:
	CInput();
	Uint32 type();
	int retMouseX();
	int retMouseY();
	~CInput();
};
