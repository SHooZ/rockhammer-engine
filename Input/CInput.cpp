#include "CInput.h"

#ifdef _Win32
#pragma comment (lib, "SDL2.lib") // for windows
#endif


CInput::CInput()
{
	SDL_StartTextInput();
}

int CInput::retMouseX()
{
	if( CInput::type() == SDL_TEXTINPUT )
				{
					int x = 0, y = 0;
					SDL_GetMouseState( &x, &y );
					return x;
				}
	return -1;
}

int CInput::retMouseY()
{
	if( CInput::type() == SDL_TEXTINPUT )
				{
					int x = 0, y = 0;
					SDL_GetMouseState( &x, &y );
					return y;
				}
	return -1;
}

Uint32 CInput::type()
{	if(SDL_PollEvent(&e) != 0)
	{
		return CInput::e.type;
	}
	return -1;
}

CInput::~CInput()
{
	//Disable text input
	SDL_StopTextInput();
}

