.PHONY: all clean 
    
all: RockHammer-Engine
clean:
	rm -rf RockHammer-Engine ./obj/*.o ./lib/*.a
RockHammer-Engine: ./Engine/CEngine.cpp
	g++ -c ./Engine/CEngine.cpp -o ./obj/Engine.o
	ar rvs ./lib/libRHEngine.a ./obj/Engine.o
