#include "CWindow.h"
#include <iostream>

#pragma comment (lib, "SDL2.lib")

CWindow::CWindow()
{
	CWindow::WIDTH = 800;
	CWindow::HEIGHT = 600;
	CWindow::TITLE = "RockHammer Engine";
	CWindow::WINDOW = NULL;
	CWindow::Surfase = NULL;
}


int CWindow::createWindow()
{
	CWindow::WINDOW = SDL_CreateWindow(TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if(WINDOW == NULL)
	{
		std::cout << "CWindow::createWindow - SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
    }
	std::cout<<"CWindow::createWindow - Window creating...\tOK"<<std::endl;
	Context = SDL_GL_CreateContext( CWindow::WINDOW );
	//Update the surface

	return 0;
}
void CWindow::update()
{
	SDL_GL_SwapWindow( CWindow::WINDOW );
}

unsigned int CWindow::getHeight()
{
	return CWindow::HEIGHT;
}

unsigned int CWindow::getWidth()
{
	return CWindow::WIDTH;
}


CWindow::~CWindow()
{
	SDL_DestroyWindow(CWindow::WINDOW);
}