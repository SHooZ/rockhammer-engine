#pragma once

#if defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) \
 || defined(__TOS_WIN__) || defined(__WINDOWS__)
/* Compiling for Windows */
#ifndef __WINDOWS__
#define __WINDOWS__
#endif
#  include <windows.h>
#endif/* Predefined Windows macros */
-=
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>

class CWindow
{
private:
	unsigned int HEIGHT;
	unsigned int WIDTH;
	char* TITLE;
	SDL_Window* WINDOW;
	SDL_Surface* Surfase;
	SDL_Renderer* Renderer;
	SDL_Rect* Viewport;
	SDL_GLContext Context;
public:
	CWindow();
	int createWindow();
	unsigned int getHeight();
	unsigned int getWidth();
	void update();
	bool changeWindowTitle(char*);
	~CWindow();
};
